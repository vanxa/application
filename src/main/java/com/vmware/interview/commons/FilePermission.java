package com.vmware.interview.commons;

/**
 * @author Ivan Konstantinov (ikonstantino@vmware.com)
 */
public enum FilePermission {
    READ, WRITE, READ
}
